state= {
	id=869
	name="STATE_869"
	manpower = 1373000
	state_category = state_03
	
	resources={
        oil=15 #Petroleum
}
	
	history={
		owner = TRI
		victory_points = { 3284 5 } #Port of Spain
		victory_points = { 13012 1 } #Tobago
		
		buildings = {
			infrastructure = 4
			arms_factory = 2
            industrial_complex = 1
			air_base = 3
			
			3284 = {
				naval_base = 3
			}

		}
		add_core_of = TRI
	}

	provinces={
		  3284 13012
		
		}
}
