
state={
	id=605
	name="STATE_605"

	history={
		owner = KOR
		victory_points = {
			4056 10 
		}
		victory_points = {
			12060 5 
		}
		victory_points = {
			4086 5 
		}
		victory_points = {
			7321 4 
		}
		victory_points = {
			1054 2 
		}
		victory_points = {
			1081 2 
		}
		victory_points = {
			4103 1 
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 7
			arms_factory = 2
			dockyard = 5
			air_base = 6
			4056 = {
				naval_base = 4

			}

		}
		add_core_of = NKO
		add_core_of = KOR

	}

	provinces={
		1054 1081 4056 4086 4103 7121 10085 10115 11912 12060 12089 14381 14383 14386 14387 
	}
	manpower=13440509
	buildings_max_level_factor=1.000
	state_category=state_09
}
