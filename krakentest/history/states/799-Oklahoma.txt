state= {
	id=799
	name="STATE_799"
	manpower = 4026580
	state_category = state_05

	history={
		owner = USA
		victory_points = { 5103 5 } #Oklahoma City
		victory_points = { 1806 5 } #Tulsa
		victory_points = { 11743 3 } #Lawton
		victory_points = { 7904 1 } #Enid
		victory_points = { 1618 1 } #Muskogee
		victory_points = { 8025 1 } #Ardmore
		
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			arms_factory = 3
			air_base = 4

		}
		add_core_of = USA
	}

	provinces={
		  1618 1806 2043 4919 5103 6803 7762 7904 7945 8025 9822 10702 10798 11743 11802 12624
		
		}
}
