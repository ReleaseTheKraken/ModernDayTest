
state={
	id=537
	name="STATE_537"

	history={
		owner = CHI
		victory_points = {
			7071 10 
		}
		victory_points = {
			11986 5 
		}
		victory_points = {
			7027 3 
		}
		victory_points = {
			4196 5 
		}
		victory_points = {
			7099 1 
		}
		victory_points = {
			1193 1 
		}
		victory_points = {
			10042 1 
		}
		victory_points = {
			10069 1 
		}
		victory_points = {
			12084 5 
		}
		victory_points = {
			10010 1 
		}
		victory_points = {
			4113 1 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 5
			dockyard = 1
			air_base = 10
			7071 = {
				naval_base = 6

			}
			4196 = {
				naval_base = 4

			}

		}
		add_core_of = CHI

	}

	provinces={
		1006 1066 1094 1110 1138 1166 1193 4053 4081 4113 4169 4196 7027 7071 7099 7131 7141 7182 9938 10010 10012 10042 10054 10069 10084 10093 10112 11961 11986 12057 12084 
	}
	manpower=39100276
	buildings_max_level_factor=1.000
	state_category=state_14
}
