state= {
	id=396
	name="STATE_396"
	manpower = 355176
	state_category = state_01

	history={
		owner = HOR
		victory_points = { 7082 5 } #Derna
		victory_points = { 1130 6 } #Tobruk
		victory_points = { 9977 1 } #Jalu
		
		buildings = {
			infrastructure = 3
			air_base = 4
			
			7082 = {
				naval_base = 2
			}
			
			1130 = {
				naval_base = 2
			}
			
		}
		add_core_of = LBA
		add_core_of = GNA
		add_core_of = GNC
		add_core_of = HOR
	}

	provinces={
		7082 9992 1130 4136 10120 4060 1060 11951 9977 4133 12079 1186 12025 13078 7206
}