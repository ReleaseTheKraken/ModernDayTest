state= {
	id=786
	name="STATE_786"
	manpower = 813060
	state_category = state_02

	history={
		owner = USA
		victory_points = { 10595 3 } #Sioux Falls
		victory_points = { 1747 3 } #Rapid City
		victory_points = { 7862 1 } #Aberdeen
		victory_points = { 12620 1 } #Pierre
		victory_points = { 12667 1 } #Brookings
		
		buildings = {
			infrastructure = 6
            industrial_complex = 1

		}
		add_core_of = USA
	}

	provinces={
		  1747 1875 3750 4839 4880 7745 7771 7862 10569 10595 12620 12667 
		
		}
}
