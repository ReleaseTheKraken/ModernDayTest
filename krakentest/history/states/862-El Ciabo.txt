state= {
	id=862
	name="STATE_862"
	manpower = 3403839
	state_category = state_04

	history={
		owner = DOM
		victory_points = { 4598 5 } #Santiago
		victory_points = { 10484 3 } #San Francisco
		
		buildings = {
			infrastructure = 4
			arms_factory = 1
            industrial_complex = 1

		}
		add_core_of = DOM
	}

	provinces={
		  4598 10484
		
		}
}
