state= {
	id=502
	name="STATE_502"
	manpower = 1974285
	state_category = state_03

	history={
		owner = BRM
		victory_points = { 3651 1 } #Toungup
		
		buildings = {
			infrastructure = 1
			air_base = 1
			
			3651 = {
				naval_base = 2
			}			

		}
		
	}

	provinces={
		3651 10182
		
		}
}