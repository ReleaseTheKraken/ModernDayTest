state={
	id=922
	name="STATE_922"
	
	history = {
		owner = BAL
		add_core_of = CAR
		add_core_of = BAL
		add_core_of = SEL
		buildings = {
			infrastructure = 1
			arms_factory = 1
		}
		victory_points = {
			13544 1 
		}
	}
	
	provinces={
		8078 11012 13543 13544 13545 13548 14536 14538 14540 14542 14543 14544 
	}
	manpower=2270284
	buildings_max_level_factor=1.000
	state_category=state_04
}
