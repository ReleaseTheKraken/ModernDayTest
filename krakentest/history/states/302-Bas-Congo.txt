
state={
	id=302
	name="STATE_302"
	resources={
		tungsten=107.000
		steel=1.000
	}

	history={
		owner = DRC
		victory_points = {
			5117 15 
		}
		victory_points = {
			13635 5 
		}
		victory_points = {
			10968 5 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			arms_factory = 1
			air_base = 6
			10968 = {
				naval_base = 5

			}

		}
		add_core_of = DRC

	}

	provinces={
		5117 10968 12925 13635 
	}
	manpower=19387445
	buildings_max_level_factor=1.000
	state_category=state_11
}
