state= {
	id=751
	name="STATE_751"
	manpower = 1088220
	state_category = state_02

	history={
		owner = NZL

		victory_points = { 2197 5 } #Christchurch
		victory_points = { 2211 3 } #Dunedin
		victory_points = { 8241 1 } #Invercargill
		victory_points = { 5195 1 } #Timaru
		victory_points = { 12632 1 } #Nelson
		
		
		
		buildings = {
			infrastructure = 6
			arms_factory = 1
            industrial_complex = 2
			air_base = 2
			
			
			2197 = {
				naval_base = 2
			}

		}
		add_core_of = NZL
	}

	provinces={
		2197 2203 2211 4819 5195 7839 8204 8212 8232 8241 10650 12632 12966 12974 12980 12988 
		
		}
}
