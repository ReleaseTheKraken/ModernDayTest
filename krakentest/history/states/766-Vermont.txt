state= {
	id=766
	name="STATE_766"
	manpower = 628408
	state_category = state_01

	history={
		owner = USA
		victory_points = { 7629 3 } #Burlington
		victory_points = { 4445 1 } #Rutland
		
		buildings = {
			infrastructure = 5
            industrial_complex = 1
			air_base = 4

		}
		add_core_of = USA
	}

	provinces={
		9675 4445 7629 13158
		
		}
}