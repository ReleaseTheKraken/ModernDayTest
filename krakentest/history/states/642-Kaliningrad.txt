state= {
	id=642
	name="STATE_642"
	manpower = 981873
	state_category = state_02

	history={
		owner = SOV

		victory_points = { 6332 5 } #Kaliningrad
		victory_points = { 395 1 } #Chernyakhovsk
		victory_points = { 348 1 } #Sovetsk
		
		
		
		buildings = {
			infrastructure = 4
			dockyard = 1
			air_base = 8
			
			
			6332 = {
				naval_base = 8
			}

		}
		add_core_of = SOV
	}

	provinces={
		3384 395 6332 11265 348 281
		
		}
}
