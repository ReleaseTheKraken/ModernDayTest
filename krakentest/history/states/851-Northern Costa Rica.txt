state= {
	id=851
	name="STATE_851"
	manpower = 1901439
	state_category = state_03

	history={
		owner = COS
		victory_points = { 13092 1 } #Liberia
		
		buildings = {
			infrastructure = 4
            industrial_complex = 1
		}
		add_core_of = COS
	}

	provinces={
		  13092
		
		}
}
