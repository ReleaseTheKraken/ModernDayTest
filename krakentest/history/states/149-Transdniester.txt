
state={
	id=149
	name="STATE_149"

	history={
		owner = PMR
		victory_points = {
			13202 5 
		}
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 2

		}
		add_core_of = MLV
		add_core_of = PMR

	}

	provinces={
		13202 13203 13204 
	}
	manpower=505153
	buildings_max_level_factor=1.000
	state_category=state_01
}
