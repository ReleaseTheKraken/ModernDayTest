
state={
	id=874
	name="STATE_874"

	history={
		owner = VEN
		victory_points = {
			1944 10 
		}
		victory_points = {
			13999 5 
		}
		victory_points = {
			2052 3 
		}
		victory_points = {
			1997 3 
		}
		victory_points = {
			12772 1 
		}
		victory_points = {
			4526 1 
		}
		victory_points = {
			5088 3 
		}
		buildings = {
			infrastructure = 4
			arms_factory = 1
            industrial_complex = 2
			air_base = 2
			1944 = {
				naval_base = 1

			}

		}
		add_core_of = VEN

	}

	provinces={
		1944 1981 1997 2052 4526 5088 7915 12772 13996 13999 14000 14001 14002 
	}
	manpower=11378001
	buildings_max_level_factor=1.000
	state_category=state_09
}
