state= {
	id=202
	name="STATE_202"
	manpower = 2431459
	state_category = state_04

	history={
		owner = LEB
		victory_points = { 792 10 } #Beirut
		
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 1
			air_base = 3
			
			792 = {
				naval_base = 4
			}

		}
		add_core_of = LEB
	}

	provinces={
		  792
		
		}
}
