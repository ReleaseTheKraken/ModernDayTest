state= {
	id=744
	name="STATE_744"
	manpower = 547112
	state_category = state_01

	history={
		owner = AST
		victory_points = { 2189 5 } #Hobart
		victory_points = { 5181 2 } #Launceston
		victory_points = { 10665 1 } #Burnie
		
		
		
		buildings = {
			infrastructure = 6
            industrial_complex = 1
			
			2189 = {
				naval_base = 2
			}

		}
		add_core_of = AST
	}

	provinces={
		2183 2189 5181 5187 10665 12645
		
		}
}