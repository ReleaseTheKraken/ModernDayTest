
state={
	id=709
	name="STATE_709"
	resources={
		steel=1.000
	}

	history={
		owner = ARM
		victory_points = {
			12320 5 
		}
		victory_points = {
			12441 1 
		}
		victory_points = {
			12380 1 
		}
		buildings = {
			infrastructure = 2
			arms_factory = 2
            industrial_complex = 1
			air_base = 4

		}
		add_core_of = ARM

	}

	provinces={
		3564 11708 12320 12380 12441 12450 13337 13338 
	}
	manpower=2803641
	buildings_max_level_factor=1.000
	state_category=state_04
}
