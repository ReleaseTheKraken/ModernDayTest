state= {
	id=599
	name="STATE_599"
	manpower = 8519725
	state_category = state_07

	history={
		owner = TAI
		victory_points = { 11914 5 } #Kaohsiung
		victory_points = { 12068 5 } #Tainan
		
		buildings = {
			infrastructure = 6
			industrial_complex = 6
			arms_factory = 2
			dockyard = 2
			air_base = 2
			
			11914 = {
				naval_base = 4
			}
			
			12068 = {
				naval_base = 2
			}


		}
		add_core_of = TAI
		add_core_of = CHI
	}

	provinces={
		1091 12068 11914
		
		}
}
