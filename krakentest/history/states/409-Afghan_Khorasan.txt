state= {
	id=409
	name="STATE_409"
	manpower = 3534471
	state_category = state_04

	history={
		owner = AFG
		victory_points = { 4893 5 } #Herat
		victory_points = { 2062 1 } #Farah
		
		buildings = {
			infrastructure = 2
			air_base = 2
			arms_factory = 1
			
		}
		add_core_of = AFG
		add_core_of = TAL

	}

	provinces={
		4893 8053 12871 2062
		
		}
}