
state={
	id=510
	name="STATE_510" 
	manpower = 4139095
	state_category = state_05

	history={
		owner = SIA
		victory_points = { 7236 5 } #Hat Yai
		victory_points = { 12131 1 } #Yala
		
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			air_base = 4
			dockyard = 1
			
			7236 = {
				naval_base = 6
			}

		}
		add_core_of = SIA

	}

	provinces={
		7236 12131 4297 1279 
	}
}


