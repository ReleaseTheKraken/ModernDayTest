state= {
	id=843
	name="STATE_843"
	manpower = 4020900
	state_category = state_05

	history={
		owner = ELS
		victory_points = { 12823 5 } #San Salvador
		
		buildings = {
			infrastructure = 3
			arms_factory = 1
            industrial_complex = 1
			air_base = 1
			
			12823 = {
				naval_base = 1
			}

		}
		add_core_of = ELS
	}

	provinces={
		  12823
		
		}
}
