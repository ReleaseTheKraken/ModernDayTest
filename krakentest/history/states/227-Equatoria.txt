
state={
	id=227
	name="STATE_227"
	resources={
		oil=4.000
	}

	history={
		owner = SSU
		victory_points = {
			13549 5 
		}
		victory_points = {
			10764 1 
		}
		victory_points = {
			7996 1 
		}
		buildings = {
			infrastructure = 0
			industrial_complex = 1
			arms_factory = 1
			air_base = 1

		}
		add_core_of = SSU
		add_core_of = AGF
		AGF = {
			set_province_controller = 14553
			set_province_controller = 14557
			set_province_controller = 14558
			set_province_controller = 14560
		}

	}

	provinces={
		1953 2096 7996 10764 10859 13549 13550 13551 13552 13553 13554 13555 14553 14556 14557 14558 14559 14560 14561 
	}
	manpower=5453664
	buildings_max_level_factor=1.000
	state_category=state_06
}
