
state={
	id=68
	name="STATE_68"
	manpower = 870000
	state_category = state_02

	history={
		owner = FRA
		victory_points = { 9377 1 } #Les Abymes
		victory_points = { 177 1 } #Fort-de-France
		buildings = {
			infrastructure = 4
			air_base = 1
			177 = {
				naval_base = 3
			}
			9377 = {
				naval_base = 1
			}
		}
		add_core_of = FRA
	}
	
	provinces={
	 9377 177 
	}
}
