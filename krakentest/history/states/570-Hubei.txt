
state={
	id=570
	name="STATE_570"

	history={
		owner = CHI
		victory_points = {
			4619 10 
		}
		victory_points = {
			1036 3 
		}
		victory_points = {
			4631 1 
		}
		victory_points = {
			12467 1 
		}
		victory_points = {
			7167 1 
		}
		victory_points = {
			10462 1 
		}
		victory_points = {
			7637 1 
		}
		victory_points = {
			4130 1 
		}
		victory_points = {
			7625 1 
		}
		victory_points = {
			10757 5 
		}
		victory_points = {
			12086 5 
		}
		victory_points = {
			4009 5 
		}
		victory_points = {
			7019 5 
		}
		victory_points = {
			9934 1 
		}
		victory_points = {
			5030 1 
		}
		victory_points = {
			12414 1 
		}
		victory_points = {
			1612 1 
		}
		victory_points = {
			7609 1 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 6
			arms_factory = 2
			air_base = 10

		}
		add_core_of = CHI

	}

	provinces={
		1036 1612 4009 4130 4619 4631 5030 5092 7019 7167 7580 7596 7609 7625 8072 9934 9959 10462 10474 10757 12086 12408 12414 12467 14346 14348 14352 14359 
	}
	manpower=59127573
	buildings_max_level_factor=1.000
	state_category=state_16
}
