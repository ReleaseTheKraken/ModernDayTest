
state={
	id=448
	name="STATE_448"

	history={
		owner = ARG
		victory_points = {
			8187 1 
		}
		victory_points = {
			11007 3 
		}
		victory_points = {
			10943 1 
		}
		victory_points = {
			10515 1 
		}
		victory_points = {
			12964 2 
		}
		victory_points = {
			12453 1 
		}
		victory_points = {
			1449 2 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			arms_factory = 1
			air_base = 3

		}
		add_core_of = ARG

	}

	provinces={
		1449 8187 8238 10515 10943 11007 12453 12964 13847 
	}
	manpower=3700465
	buildings_max_level_factor=1.000
	state_category=state_04
}
