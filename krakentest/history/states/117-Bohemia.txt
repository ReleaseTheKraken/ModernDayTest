
state={
	id=117
	name="STATE_117"
	resources={
		steel=2.000
	}

	history={
		owner = CZH
		victory_points = {
			11542 10 
		}
		victory_points = {
			6418 1 
		}
		victory_points = {
			11401 1 
		}
		victory_points = {
			9541 1 
		}
		victory_points = {
			11432 1 
		}
		victory_points = {
			494 1 
		}
		buildings = {
			infrastructure = 7
			arms_factory = 4
			industrial_complex = 5
			air_base = 5

		}
		add_core_of = CZH

	}

	provinces={
		424 445 469 494 543 3418 3462 3585 5283 6418 6440 6470 6592 9414 9421 9429 9541 9555 9569 11401 11414 11432 11542 13314 13315 
	}
	manpower=6208749
	buildings_max_level_factor=1.000
	state_category=state_06
}
