
state={
	id=30
	name="STATE_30"
	manpower = 1406580

	state_category = state_03



	history={
		owner = NOR
		victory_points = { 122 5 } #Bergen
		victory_points = { 6145 1 } #Voss
		victory_points = { 65 1 } #Odda
		victory_points = { 53 5 } #Stavanger
		victory_points = { 6176 1 } #Egersund
		victory_points = { 3192 3 } #Haugesund
		victory_points = { 3090 1 } #Florø
		victory_points = { 199 1 } #Førde
		victory_points = { 3188 1 } #Sogndal
		victory_points = { 9127 3 } #Ålesund
		victory_points = { 11100 1 } #Molde
		victory_points = { 9216 1 } #Kristiansund
		
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			arms_factory = 1
			dockyard = 2
			air_base = 1

			53 = {
				naval_base = 2
			}
			3090 = {
				naval_base = 1
			}
			9127 = {
				naval_base = 1
			}
			122 = {
				naval_base = 6
			}
		}
		add_core_of = NOR
	}

	provinces={
		53 65 102 122 137 188 199 3049 3090 3093 3096 
		3114 3129 3180 3188 3192 6145 6176 6204 6216 
		9047 9127 9170 9216 11100 11101 11198 11367 
	}
}
