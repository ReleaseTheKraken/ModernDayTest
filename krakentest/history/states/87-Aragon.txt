state={
	id=87
	name="STATE_87"
	manpower = 1345473
	
	state_category = state_02

	history={
		owner = SPR
		victory_points = { 3816 5 } #Zaragoza
		
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 3
		}
		add_core_of = SPR
	}

	provinces={
		790 798 813 871 899 915 948 3816 3909 3954 6878 
		6901 7213 9812 9813 9840 9842 11737 11793 11819 
		11821 11838 
	}
}
