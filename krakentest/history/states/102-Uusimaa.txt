
state={
	id=102
	name="STATE_102"
	resources={
		tungsten=3.000
		steel=7.000
	}

	history={
		owner = FIN
		victory_points = {
			11105 10 
		}
		victory_points = {
			3083 5 
		}
		victory_points = {
			67 1 
		}
		victory_points = {
			9167 1 
		}
		victory_points = {
			6144 1 
		}
		victory_points = {
			204 1 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 5
			industrial_complex = 2
			air_base = 5
			11105 = {
				naval_base = 5

			}
			3083 = {
				naval_base = 3

			}

		}
		add_core_of = FIN

	}

	provinces={
		67 169 204 3083 3161 6009 6066 6108 6144 6163 9073 9094 9129 9167 9194 11066 11105 11138 11172 11211 
	}
	manpower=2500909
	buildings_max_level_factor=1.000
	state_category=state_04
}
