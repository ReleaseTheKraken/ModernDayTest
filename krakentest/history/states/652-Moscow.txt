state= {
	id=652
	name="STATE_652"
	manpower = 19728664
	state_category = state_11
	
	resources={
        oil=369 #Petroleum
	    steel=43 #Steel
        aluminium=41 #Light Metals
        tungsten=28 #Technology Metals
}

	history={
		owner = SOV

		victory_points = { 6380 15 } #Moscow
		victory_points = { 6414 3 } #Podolsk
		victory_points = { 301 3 } #Balashikha
		victory_points = { 6283 1 } #Sergiyev Posad
		victory_points = { 629 1 } #Serpukhov
		victory_points = { 9282 3 } #Kolomna
		victory_points = { 3358 1 } #Klin
		
		buildings = {
			infrastructure = 6
			industrial_complex = 4
			arms_factory = 6
			air_base = 10

		}
		add_core_of = SOV
	}

	provinces={
		301 350 366 401 3259 3290 3327 3358 3391 6283 6290 6348 6380 6414 9235 9282 9302 9348 9378 9395 11217 11236 11268 11282 11330
		
		}
}



