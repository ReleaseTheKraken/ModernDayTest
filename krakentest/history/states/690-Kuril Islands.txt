state= {
	id=690
	name="STATE_690"
	manpower = 19434
	state_category = state_00

	history={
		owner = SOV

		victory_points = { 13037 1 } #Yuzhno-Kurilsk
		victory_points = { 1399 1 } #Severo-Kurilsk

		
		buildings = {
			infrastructure = 3
			
			13037 = {
				naval_base = 2
			}
			
			1399 = {
				naval_base = 1
			}

		}
		add_core_of = SOV
		add_core_of = JAP
	}

	provinces={
        1399 1515 13037 
		
		}
}
