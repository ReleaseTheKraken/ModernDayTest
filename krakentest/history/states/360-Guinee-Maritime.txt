
state={
	id=360
	name="STATE_360"
	resources={
		aluminium=36.000
	}

	history={
		owner = GUI
		victory_points = {
			1909 5 
		}
		victory_points = {
			12858 1 
		}
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			air_base = 1
			1909 = {
				naval_base = 2

			}

		}
		add_core_of = GUI

	}

	provinces={
		1909 12802 12858 13451 
	}
	manpower=8480622
	buildings_max_level_factor=1.000
	state_category=state_07
}
