state= {
	id=803
	name="STATE_803"
	manpower = 2110310
	state_category = state_03

	history={
		owner = USA
		victory_points = { 4975 5 } #Albuquerque
		victory_points = { 12791 3 } #Las Cruces
		victory_points = { 8070 1 } #Roswell
		victory_points = { 12720 1 } #Hobbs
		victory_points = { 10867 1 } #Clovis
		victory_points = { 10645 1 } #Farmington
		victory_points = { 3867 1 } #Carlsbad
		
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			arms_factory = 2
			air_base = 10

		}
		add_core_of = USA
	}

	provinces={
		  1972 2102 3867 3883 4975 5044 8070 10642 10645 10867 10914 12552 12684 12720 12791
		}
}
