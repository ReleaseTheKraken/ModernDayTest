state= {
	id=857
	name="STATE_857"
	manpower = 11267
	state_category = state_00

	history={
		owner = BAH
		victory_points = { 1513 1 } #George Town
		
		buildings = {
			infrastructure = 5
			
			1513 = {
				naval_base = 1
			}

		}
		add_core_of = BAH
	}

	provinces={
		  1513
		
		}
}
