state= {
	id=568
	name="STATE_568"
	manpower = 11640
	state_category = state_00

	history={
		owner = PLY
		victory_points = { 13054 1 } #Nanumea
		victory_points = { 13055 1 } #Nukufetau
		victory_points = { 13056 3 } #Funafuti
		victory_points = { 13057 1 } #Nukulaelae
		
		buildings = {
			infrastructure = 3
			air_base = 1
			
			13056 = {
				naval_base = 1
			}
			
			13054 = {
				naval_base = 1
			}

		}
		add_core_of = PLY
	}

	provinces={
		 13054 13055 13056 13057
		
		}
}
