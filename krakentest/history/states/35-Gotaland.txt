
state={
	id=35
	name="STATE_35"

	history={
		owner = SWE
		victory_points = {
			383 5 
		}
		victory_points = {
			6331 2 
		}
		victory_points = {
			9329 2 
		}
		victory_points = {
			9393 4 
		}
		victory_points = {
			11380 2 
		}
		victory_points = {
			242 1 
		}
		victory_points = {
			299 1 
		}
		victory_points = {
			11070 1 
		}
		victory_points = {
			69 1 
		}
		victory_points = {
			3375 1 
		}
		victory_points = {
			295 1 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 4
			arms_factory = 1
			air_base = 5
			dockyard = 1
			11380 = {
				naval_base = 1

			}

		}
		add_core_of = SWE

	}

	provinces={
		69 171 211 242 295 299 329 363 383 3063 3229 3286 3375 3386 6148 6310 6311 6331 6397 6406 9251 9308 9329 9393 9406 9410 11070 11231 11280 11289 11311 11380 11390 
	}
	manpower=2937605
	buildings_max_level_factor=1.000
	state_category=state_04
}
