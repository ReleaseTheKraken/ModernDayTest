state= {
	id=682
	name="STATE_682"
	manpower = 5670671
	state_category = state_06

	history={
		owner = SOV

		victory_points = { 7757 5 } #Kemerovo
		victory_points = { 1369 5 } #Novokuznetsk
		victory_points = { 4665 3 } #Leninsk-Kuznetsky
		victory_points = { 1685 3 } #Mezhdurechensk
		victory_points = { 4789 3 } #Anzhero-Sudzhensk
		victory_points = { 4793 1 } #Mariinsk
		victory_points = { 10649 3 } #Abakan
		victory_points = { 4724 5 } #Krasnoyarsk
		victory_points = { 7815 3 } #Achinsk
		victory_points = { 12691 3 } #Zheleznogorsk
		victory_points = { 12621 1 } #Kansk
		victory_points = { 12563 3 } #Minusinsk
		victory_points = { 10712 1 } #Sharypovo
		victory_points = { 4711 1 } #Sayanogorsk

		
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 1
			air_base = 8

		}
		add_core_of = SOV
	}

	provinces={
        1369 1685 1702 1816 4665 4789 4793 7757 7822 7886 10580 10649 12631
		
		1777 1850 4690 4711 4724 4782 4818 4853 7744 7815 10639 10712 12563 12621 12628 12691
		
		
		}
}

#1 - Kemerovo
#2 - Southern Krasnoyarsk