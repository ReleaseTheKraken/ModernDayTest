
state={
	id=814
	name="STATE_814"

	history={
		owner = USA
		victory_points = {
			13091 5 
		}
		victory_points = {
			14127 3 
		}
		victory_points = {
			13069 3 
		}
		victory_points = {
			14125 1 
		}
		victory_points = {
			4833 1 
		}
		victory_points = {
			7884 1 
		}
		victory_points = {
			1830 1 
		}
		victory_points = {
			7855 1 
		}
		victory_points = {
			10694 1 
		}
		buildings = {
			infrastructure = 4
            industrial_complex = 1
			air_base = 10
			13091 = {
				naval_base = 4

			}
			14127 = {
				naval_base = 1

			}
			7884 = {
				naval_base = 1

			}
			7855 = {
				naval_base = 1

			}
			10694 = {
				naval_base = 1

			}

		}
		add_core_of = USA

	}

	provinces={
		1135 1830 4833 4848 7855 7884 10679 10694 10707 12661 13069 13090 13091 14125 14126 14127 14128 13067
	}
	manpower=764212
	buildings_max_level_factor=1.000
	state_category=state_01
}
