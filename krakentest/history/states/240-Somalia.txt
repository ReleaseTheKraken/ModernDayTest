
state={
	id=240
	name="STATE_240"

	history={
		owner = SOM
		victory_points = {
			10928 7 
		}
		victory_points = {
			12941 5 
		}
		victory_points = {
			13599 1 
		}
		victory_points = {
			11014 1 
		}
		victory_points = {
			2020 1 
		}
		buildings = {
			infrastructure = 0
			industrial_complex = 1
			arms_factory = 1
			air_base = 2
			12991 = {
				naval_base = 5

			}

		}
		add_core_of = SOM
		add_core_of = SHB
		SHB = {
			set_province_controller = 14587
			set_province_controller = 13600
			set_province_controller = 13597
			set_province_controller = 14586
		}

	}

	provinces={
		2020 2063 5065 8164 10928 11014 12941 12991 13596 13597 13598 13599 13600 13601 14586 14587 
	}
	manpower=4502606
	buildings_max_level_factor=1.000
	state_category=state_05
}
