
state={
	id=223
	name="STATE_223"

	history={
		owner = SRF
		victory_points = {
			2016 5 
		}
		victory_points = {
			14578 5
		}
		victory_points = {
			10851 5
		}
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			arms_factory = 1

		}
		add_core_of = SUD
		add_core_of = SRF
		
		SUD = {
			set_province_controller = 14574
			set_province_controller = 14575
			set_province_controller = 10827
			set_province_controller = 14576
			set_province_controller = 2016
			set_province_controller = 8084
			set_province_controller = 7925
			set_province_controller = 4961
			set_province_controller = 10900
		}

	}

	provinces={
		2016 4961 7925 8084 10827 10851 10900 13567 14574 14575 14576 14578 
	}
	manpower=5350204
	buildings_max_level_factor=1.000
	state_category=state_06
}
