state= {
	id=660
	name="STATE_660"
	manpower = 3310562
	state_category = state_04

	history={
		owner = SOV

		victory_points = { 11375 5 } #Nizhny Novgorod
		victory_points = { 9389 3 } #Dzerzhinsk
		victory_points = { 11344 3 } #Arzamas
		victory_points = { 6410 1 } #Vyksa
		victory_points = { 9407 3 } #Sarov
		victory_points = { 9172 1 } #Shakhunya
		victory_points = { 270 1 } #Semyonov
		victory_points = { 6299 1 } #Pavlovo

		
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			arms_factory = 3
			air_base = 2

		}
		add_core_of = SOV
	}

	provinces={
		9050 112 9172 3004
		9270 11134
		12 256 270 297 368 3224 3245 3261 3285 3303 3349 3352 3383 6149 6295 6299 6327 6370 6374 6410 9291 9298 9325 9358 9360 9368 9389 9407 11275 11277 11324 11340 11344 11375 11394 
		
		
		}
}
