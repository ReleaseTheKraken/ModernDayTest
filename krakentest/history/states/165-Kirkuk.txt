state= {
	id=165
	name="STATE_165"
	manpower = 1542689 
	state_category = state_03

	history={
		owner = KUR
		victory_points = { 8123 5 } #Kirkuk
		
		buildings = {
			infrastructure = 4
		}
		add_core_of = IRQ
		add_core_of = KUR
	}

	provinces={
		8123  		
	}
}
