
state={
	id=96
	name="STATE_96"
	resources={
		oil=2.000
		tungsten=6.000
	}

	history={
		owner = POR
		victory_points = {
			11805 10 
		}
		victory_points = {
			970 1 
		}
		victory_points = {
			6880 1 
		}
		victory_points = {
			3846 1 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 2
			industrial_complex = 4
			air_base = 5
			11805 = {
				naval_base = 8

			}
			3790 = {
				naval_base = 2

			}

		}
		add_core_of = POR

	}

	provinces={
		844 924 970 980 3790 3846 3866 3886 3926 6880 6884 6933 6986 9765 9819 9869 11768 11805 13253 
	}
	manpower=4344425
	buildings_max_level_factor=1.000
	state_category=state_05
}
