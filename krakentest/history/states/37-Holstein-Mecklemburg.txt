state= {
	id=37
	name="STATE_37"
	manpower = 4702043

	state_category = state_05
	
	history=
	{
		owner = GER
		victory_points = { 9347 10 } #Hamburg
		victory_points = { 6389 5 } #Kiel
		victory_points = { 11331 3 } #Lübeck
		
		buildings = {
			infrastructure = 7
			industrial_complex = 3
			arms_factory = 1
			dockyard = 2
			air_base = 4
			6389 ={
				naval_base = 8
			}
			
		}
		add_core_of = GER
	}

	provinces= {
		317 3231 3368 6257 6389 9320 9347 11331 11366
 	}
}

