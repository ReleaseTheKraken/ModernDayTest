﻿name="KrakenTest"
path="mod/krakentest/"
tags={
	"Map"
	"Gameplay"
	"Graphics"
	"Military"
}
supported_version="1.5.3"

replace_path = "history/states"
replace_path = "history/units"
replace_path = "history/countries"
replace_path = "common/countries"
replace_path = "map/strategicregions"
replace_path = "map/supplyareas"
replace_path = "portraits"
