
strategic_region={
	id=91
	name="STRATEGICREGION_91"
	provinces={
		1473 1891 1932 1969 1975 2056 2081 3123 3181 4897 4938 4972 4978 4997 5028 8028 8109 9152 10758 10780 10805 12707 12762 12817 12843 12874 13138 13488 13490 13512 13513 13514 13515 13516 13517 13518 13519 13520 13521 13522 13539 13540 13541 13542 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
	}
}
