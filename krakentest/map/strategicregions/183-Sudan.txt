
strategic_region={
	id=183
	name="STRATEGICREGION_183"
	provinces={
		1914 1953 1960 1977 2003 2016 2046 2088 2096 4033 4138 4918 4961 4963 5060 7925 7987 7989 7996 8052 8084 8103 8105 10739 10743 10745 10764 10784 10795 10827 10851 10857 10859 10877 10900 10902 10908 12725 12744 12796 12800 12806 12826 12836 12862 12881 12887 13226 13549 13550 13551 13552 13553 13554 13555 13556 13557 13558 13559 13560 13561 13562 13563 13564 13565 13566 13567 13568 14553 14554 14555 14556 14557 14558 14559 14560 14561 14562 14563 14564 14565 14566 14567 14568 14569 14570 14571 14572 14573 14574 14575 14576 14577 14578 14579 14580 14581 14582 14583 14584 14585 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 20.0 35.0 }
			temperature_day_night={ -15.0 2.0 }
			no_phenomenon=0.750
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.100
			min_snow_level=0.000
		}
	}
}
